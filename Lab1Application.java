package ru.ulstu.ru.Lab1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Lab1Application {

	public static void main(String[] args) {
		SpringApplication.run(Lab1Application.class, args);
	}
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name)
	{
		return String.format("Hello %s!", name);
	}
	@GetMapping("/task")
	public String str(@RequestParam(value = "str1") String str1,
	@RequestParam(value = "m1") Character m1,
	@RequestParam(value = "m2") Character m2)
	{

		return str1.replace(m1, m2);
	}
}
